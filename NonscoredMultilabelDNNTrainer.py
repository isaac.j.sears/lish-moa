import numpy as np

from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD, Adam
from sklearn.metrics import log_loss
from sklearn.model_selection import KFold

from lishmoautil.DataUtil import *
from lishmoautil.Globals import *


def get_multilabel_dnn(X, Y):
    model = Sequential()
    model.add(BatchNormalization(input_dim=np.shape(X)[1]))
    model.add(Dense(512, activation='elu'))

    for idx in range(0, 11):
        model.add(BatchNormalization())
        model.add(Dense(256, activation='elu'))
        model.add(BatchNormalization())

    model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

    adm = Adam()
    model.compile(loss='binary_crossentropy', optimizer=adm)

    return model


def predict_avg_ensemble(model_list, X):
    predictions = list()
    for model in model_list:
        y_hat = model.predict(X)
        assert np.shape(y_hat)[0] == np.shape(X)[0], "[-] Prediction matrix dimensional mismatch"
        predictions.append(np.copy(y_hat))

    y_hat_averaged = np.mean(predictions, axis=0)
    assert np.shape(y_hat_averaged)[0] == np.shape(X)[0], "[-] Prediction matrix dimensional mismatch"

    return y_hat_averaged


X = extract_x(f'{DATA_ROOT}/train_features.csv')
Y = extract_y(f'{DATA_ROOT}/train_targets_nonscored.csv')

assert np.shape(X)[0] == np.shape(Y)[0]

print("[+] Extraction complete")
print(f'X shape: {np.shape(X)}')
print(f'\t{np.shape(X)[0]} training examples with {np.shape(X)[1]} features')
print(f'Y shape: {np.shape(Y)}')
print(f'\t{np.shape(Y)[0]} training examples with {np.shape(Y)[1]} possible labels')

kfold = KFold(n_splits=5, shuffle=True)
nonscored_tgt_predictors = list()

for idx, (train, test) in enumerate(kfold.split(X, Y)):
    nonscored_tgt_predictors.append(get_multilabel_dnn(X, Y))

    history = nonscored_tgt_predictors[idx].fit(
        X[train],
        Y[train],
        epochs=50,
        batch_size=128,
        verbose=1,
        validation_data=(X[test], Y[test]),
        callbacks=[
            EarlyStopping(patience=3, min_delta=1e-5, restore_best_weights=True),
            ReduceLROnPlateau(patience=1, factor=0.1, verbose=1, mode="auto")
        ]
    )

    # score = curr_model.evaluate(X[test], Y[test])
    y_hat = nonscored_tgt_predictors[idx].predict(X[test])
    score = my_log_loss(Y[test], y_hat)
    nonscored_tgt_predictors[idx].save(f'{MODEL_ROOT}/nonscored-multilabeldnn/model-{idx}.h5')

    print(f'[+] Done with fold {idx}, final validation score: {score}')


print("[+] Running sanity check on nonscore-target predictors")
x_sc = extract_x(f'{DATA_ROOT}/train_features.csv')
y_actual_sc = extract_y(f'{DATA_ROOT}/train_targets_nonscored.csv')
y_hat_sc = predict_avg_ensemble(nonscored_tgt_predictors, x_sc)

loss_sc = my_log_loss(y_actual_sc, y_hat_sc)
print(f"[*] Sanity check loss: {loss_sc}")