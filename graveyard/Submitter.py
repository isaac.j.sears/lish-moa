import os
import sys

if not os.path.exists('../lishmoautil'):  # Hack to allow submitter to find libs during kaggle execution
    sys.path.append('/kaggle/input/')

import pandas as pd
import numpy as np

from tensorflow import keras
from lishmoautil import Globals
from lishmoautil.DataUtil import extract_x, extract_y, write_submission
from lishmoautil.Evaluations import evaluate
from lishmoautil.MultilabelDNN import MultilabelDNN


CV_COUNT = 5


def scoreCSV(submission_path):
    Y_actual = extract_y(f'{Globals.DATA_ROOT}/train_targets_scored.csv')
    Y_hat = extract_y(submission_path)

    return evaluate(Y_actual, Y_hat)


def doSubmit(nonscored_model, nonscored_ensemble_model, X_csv_path, submission_path):
    X = extract_x(X_csv_path)
    print(f'\t{np.shape(X)[0]} examples with {np.shape(X)[1]} features')
    Y_nonscored_hat = nonscored_model.cv_ensemble_predict(X)
    X_augmented = np.concatenate((X, Y_nonscored_hat), axis=1)

    Y_hat = nonscored_ensemble_model.cv_ensemble_predict(X_augmented)
    write_submission(Y_hat, X_csv_path, f'{Globals.DATA_ROOT}/sample_submission.csv', submission_path)



# TODO: Fixed a major bug here, should try nonscored again
if __name__ == '__main__':

    print("[*] Loading pretrained models...")
    pretrained_nonscored_models = [
        keras.models.load_model(f'{Globals.MODEL_ROOT}/nonscored-multilabeldnn/model-{idx}.h5') for idx in
        range(0, CV_COUNT)
    ]
    nonscored_model = MultilabelDNN("nonscored-multilabeldnn", pretrained_models=pretrained_nonscored_models)

    pretrained_nonscored_ensemble_models = [
        keras.models.load_model(f'{Globals.MODEL_ROOT}/nonscored-ensemble-multilabeldnn/model-{idx}.h5') for idx in
        range(0, CV_COUNT)
    ]
    nonscored_ensemble_model = MultilabelDNN("nonscored-ensemble-multilabeldnn",
                                             pretrained_models=pretrained_nonscored_ensemble_models)

    print("[*] Running sanity check on test data")
    doSubmit(
        nonscored_model,
        nonscored_ensemble_model,
        f'{Globals.DATA_ROOT}/train_features.csv',
        '../sanitycheck.csv'
    )

    print("[+] Sanity check ran without errors, calculating score")
    print(f"[+] Sanity check score {scoreCSV('../sanitycheck.csv')}")

    print("[*] Predicting actual targets...")
    doSubmit(
        nonscored_model,
        nonscored_ensemble_model,
        f'{Globals.DATA_ROOT}/test_features.csv',
        '../submission.csv'
    )

    print("[+] Done!")
