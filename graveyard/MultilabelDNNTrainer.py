from lishmoautil import Globals
from lishmoautil.MultilabelDNN import MultilabelDNN

import numpy as np

if __name__ == '__main__':
    print("[*] Loading cache...")
    X = np.load(f'{Globals.CACHE_PATH}/X.npy', allow_pickle=False)
    Y = np.load(f'{Globals.CACHE_PATH}/Y.npy', allow_pickle=False)

    scored_model = MultilabelDNN("scored-multilabeldnn")
    scored_model.cross_validate(X, Y)