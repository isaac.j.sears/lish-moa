from lishmoautil import Globals
from lishmoautil.MultilabelDNN import MultilabelDNN

import numpy as np

if __name__ == '__main__':
    print("[*] Loading cache...")
    X = np.load(f'{Globals.CACHE_PATH}/X.npy', allow_pickle=False)
    Y_scored = np.load(f'{Globals.CACHE_PATH}/Y.npy', allow_pickle=False)
    Y_nonscored = np.load(f'{Globals.CACHE_PATH}/Y_nonscored.npy', allow_pickle=False)

    nonscored_model = MultilabelDNN(name="nonscored-multilabeldnn")
    nonscored_model.params['depth'] = 5  # Shallow for nonscored
    nonscored_model.cross_validate(X, Y_nonscored)
    # Could just use actual values, but predicting for consistency with submission workflow
    Y_nonscored_hat = nonscored_model.cv_ensemble_predict(X)
    X_augmented = np.concatenate((X, Y_nonscored_hat), axis=1)

    nonscored_ensemble_model = MultilabelDNN(name="nonscored-ensemble-multilabeldnn")
    nonscored_ensemble_model.params['depth'] = 10  # Deep for everything else
    nonscored_ensemble_model.cross_validate(X_augmented, Y_scored)

