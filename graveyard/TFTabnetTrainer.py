import numpy as np
import tensorflow as tf
from sklearn.model_selection import KFold

from tabnet import TabNet, TabNetClassifier

CACHE_PATH = '/zfs-data-pool/kaggle/lish-moa/cache'


print("[*] Loading cache...")
X = np.load(f'{CACHE_PATH}/X.npy', allow_pickle=False)
Y = np.load(f'{CACHE_PATH}/Y.npy', allow_pickle=False)


assert np.shape(X)[0] == np.shape(Y)[0]

print("[+] Extraction complete")
print(f'X shape: {np.shape(X)}')
print(f'\t{np.shape(X)[0]} training examples with {np.shape(X)[1]} features')
print(f'Y shape: {np.shape(Y)}')
print(f'\t{np.shape(Y)[0]} training examples with {np.shape(Y)[1]} possible labels')

print("[*] Running k-fold cross validation")
kfold = KFold(n_splits=5, shuffle=True)
# kfold = MultilabelStratifiedKFold(n_splits=self.validation_splits, shuffle=True)
fold_idx = 0

for train, test in kfold.split(X, Y):
    curr_model = TabNetClassifier(
        feature_columns=None,
        feature_dim=32,
        output_dim=31,
        num_classes=np.shape(Y)[1],
        num_features=np.shape(X)[1],
        virtual_batch_size=64
    )

    lr = tf.keras.optimizers.schedules.ExponentialDecay(0.02, decay_steps=500, decay_rate=0.9, staircase=False)
    optimizer = tf.keras.optimizers.Adam(lr)
    #optimizer = tf.keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    curr_model.compile(optimizer, loss='binary_crossentropy')

    curr_model.fit(X[train], Y[train], validation_data=(X[test], Y[test]), epochs=100)

    fold_idx += 1

