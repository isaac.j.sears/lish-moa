import optuna

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from lishmoautil.ModelUtil import cross_validate_nonscored, SimpleTfModel


def build_model(width, depth, optimizer, hidden_activation, input_dim=875, output_dim=206):
    model = Sequential()
    model.add(BatchNormalization(input_dim=input_dim))
    model.add(Dense(width, activation=hidden_activation))

    for idx in range(0, depth):
        model.add(BatchNormalization())
        model.add(Dense(width, activation=hidden_activation))
        model.add(BatchNormalization())

    model.add(Dense(output_dim, activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer=optimizer)

    return model


def objective(trial):
    #batch_size = trial.suggest_categorical('batch_size', [16, 32, 64, 100, 128, 200, 256, 512])
    batch_size = 128
    es_patience = trial.suggest_int('es_patience', 3, 15)
    rlr_patience = trial.suggest_int('rlr_patience', 1, es_patience - 1)
    rlr_factor = trial.suggest_uniform('rlr_factor', 0.01, 0.99)

    fit_params = dict(
        epochs=500,
        batch_size=batch_size,
        verbose=0,
        callbacks=[
            EarlyStopping(patience=es_patience, min_delta=1e-6, restore_best_weights=True),
            ReduceLROnPlateau(patience=rlr_patience, factor=rlr_factor, verbose=0, mode="auto")
        ]
    )

    optimizer_type = trial.suggest_categorical('optimizer_type', ['SGD', 'Adam'])

    if optimizer_type == 'SGD':
        optimizer = SGD(
            learning_rate=trial.suggest_loguniform('learning_rate', 1e-7, 1e-1),
            momentum=trial.suggest_categorical('momentum', [0, 0.01, 0.1, 0.5, 0.9]),
            nesterov=trial.suggest_categorical('nesterov', [True, False])
        )

    elif optimizer_type == 'Adam':
        optimizer = Adam(
            learning_rate=trial.suggest_loguniform('learning_rate', 1e-7, 1e-1),
            epsilon=trial.suggest_loguniform('epsilon', 1e-9, 1e-4),
            amsgrad=trial.suggest_categorical('amsgrad', [True, False])
        )
    else:
        optimizer = None


    build_params = dict(
        width=trial.suggest_int('width', 10, 1024),
        depth=trial.suggest_int('depth', 1, 20),
        optimizer=optimizer,
        hidden_activation=trial.suggest_categorical('hidden_activation', ['relu', 'elu']),
        input_dim=875 + 402
    )

    model_factory = lambda: build_model(**build_params)
    model_factory_nonscored = lambda: build_model(width=256, depth=4, optimizer=Adam(), hidden_activation='elu', output_dim=402)

    stm = SimpleTfModel(fit_params=fit_params, model_factory=model_factory)
    stm_nonscored = SimpleTfModel(fit_params=fit_params, model_factory=model_factory_nonscored)
    score = cross_validate_nonscored(model=stm, nonscored_model=stm_nonscored, folds=5, seeds=[1337])

    return score


if __name__ == '__main__':
    study = optuna.create_study(
        study_name='NonscoredEnsemble',
        storage='mysql://root:root@localhost/optuna',
        load_if_exists=True
    )

    study.optimize(objective, n_trials=500, n_jobs=1)