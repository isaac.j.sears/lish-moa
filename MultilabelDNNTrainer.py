from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from lishmoautil.ModelUtil import cross_validate, SimpleTfModel


def get_model():
    model = Sequential()
    model.add(BatchNormalization(input_dim=875))
    model.add(Dense(875, activation='elu'))

    for idx in range(0, 11):
        model.add(BatchNormalization())
        model.add(Dense(11, activation='elu'))
        model.add(BatchNormalization())

    model.add(Dense(206, activation='sigmoid'))

    adm = Adam(learning_rate=0.01, epsilon=0.01, amsgrad=True)
    model.compile(loss='binary_crossentropy', optimizer=adm)

    return model


if __name__ == '__main__':
    fit_params = dict(
        epochs=500,
        batch_size=128,
        verbose=0,
        callbacks=[
            EarlyStopping(patience=9, min_delta=1e-5, restore_best_weights=True),
            ReduceLROnPlateau(patience=1, factor=0.9, verbose=0, mode="auto"),
        ]
    )

    cross_validate(SimpleTfModel(fit_params, get_model), 10)