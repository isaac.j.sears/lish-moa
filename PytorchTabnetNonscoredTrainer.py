import numpy as np
import torch
import torch.nn.functional as F
from sklearn.model_selection import KFold
from pytorch_tabnet.tab_model import TabNetRegressor
from lishmoautil.Globals import *
from lishmoautil.TabNetMetric import CustomMetric


def train(params):
    # print("[*] Loading cache...")
    X = np.load(f'{CACHE_PATH}/X.npy', allow_pickle=False)
    Y = np.load(f'{CACHE_PATH}/Y_nonscored.npy', allow_pickle=False)


    assert np.shape(X)[0] == np.shape(Y)[0]

    print("[+] Extraction complete")
    print(f'X shape: {np.shape(X)}')
    print(f'\t{np.shape(X)[0]} training examples with {np.shape(X)[1]} features')
    print(f'Y shape: {np.shape(Y)}')
    print(f'\t{np.shape(Y)[0]} training examples with {np.shape(Y)[1]} possible labels')

    print("[*] Running k-fold cross validation")
    kfold = KFold(n_splits=5, shuffle=True)
    fold_idx = 0

    cv_scores = list()
    train_scores = list()

    metric = CustomMetric()

    for train, test in kfold.split(X, Y):
        curr_model = TabNetRegressor(**params)

        trainY = Y[train]
        trainX = X[train]
        history = curr_model.fit(
            X_train=trainX, y_train=trainY,
            eval_set=[(X[test], Y[test])],
            eval_name=['val'],
            eval_metric=[CustomMetric],
            patience=15,
            batch_size=1024,
            virtual_batch_size=32,
            loss_fn=F.binary_cross_entropy_with_logits,
            max_epochs=1000
        )

        curr_model.save_model(f'{MODEL_ROOT}/tabnet-ns-predictor-{fold_idx}')

        final_y_pred = curr_model.predict(X[test])
        final_y_train_pred = curr_model.predict(X[train])

        cv_score = metric.__call__(Y[test], final_y_pred)
        train_score = metric.__call__(Y[train], final_y_train_pred)

        cv_scores.append(cv_score)
        train_scores.append(train_score)

        print(f'Score for fold {fold_idx}: {cv_score}')
        fold_idx += 1

    avg_cv_score = np.average(np.array(cv_scores))
    avg_train_score = np.average(np.array(train_scores))
    print(f'CV complete. Average score: {avg_cv_score}')
    return avg_cv_score, avg_train_score


if __name__ == '__main__':
    train(dict(
        n_d=32,
        n_a=32,
        lambda_sparse=1e-6,
        optimizer_fn=torch.optim.Adam,
        optimizer_params={"lr": 0.1},
        mask_type='entmax',  # Default sparsemax
        scheduler_params={
            "mode": "min",
            "patience": 15,
            "min_lr": 1e-7,
            "factor": 0.9
        },
        scheduler_fn=torch.optim.lr_scheduler.ReduceLROnPlateau,
        verbose=10,
        cat_idxs=[0, 2],
        cat_dims=[2, 2]
    ))