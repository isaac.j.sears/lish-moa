LINES_TO_GRAB = 100
REAL_DATA_ROOT = '/Users/isears/Repos/kaggle/MoAPrediction/data'

files_of_interest = [
    'test_features.csv',
    'train_features.csv',
    'train_targets_nonscored.csv',
    'train_targets_scored.csv',
    'sample_submission.csv'
]

for fname in files_of_interest:
    with open(f'{REAL_DATA_ROOT}/{fname}', 'r') as f_in:
        all_lines = f_in.readlines()

        if LINES_TO_GRAB < len(all_lines):
            head = all_lines[0:LINES_TO_GRAB]
        else:
            head = all_lines

        with open(f'./{fname}', 'w') as f_out:
            f_out.writelines(head)