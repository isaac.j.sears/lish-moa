import random
import mlflow

from lishmoautil.DataUtil import *
from lishmoautil.Globals import *
from MultilabelDNNTrainer import *


mlflow.set_tracking_uri("http://192.168.45.105:5000")
mlflow.set_experiment("lish-moa-dnn-optimizing")


X = extract_x(f'{DATA_ROOT}/train_features.csv')
Y = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

while True:
    mlflow.start_run()

    optimizer_params = dict(
        learning_rate=random.choice([0.5, 0.25, 0.15, 0.1, 0.01, 0.001, 0.0001, 0.00001]),
        epsilon=random.choice([1e-9, 1e-8, 1e-7, 5e-7, 9e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1]),
        amsgrad=random.choice([True, False])
    )

    es_params = dict(
        patience=random.choice([2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 50, 100]),
        min_delta=random.choice([1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2])
    )

    rlr_params = dict(
        patience=random.randint(1, es_params['patience'] - 1),
        factor=random.choice([0.999, 0.99, 0.9, 0.7, 0.5, 0.3, 0.1, 0.01, 0.001])
    )

    batch_size = random.choice([2, 10, 16, 32, 64, 100, 128, 512, 1024, 2048])
    width = random.randint(10, 2 * np.shape(X)[1])
    depth = random.randint(2, 25)
    #folds = random.choice([2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 50])
    folds = 5 # In theory, we can manipulate this variable independently after we've identified the optimal architecture

    try:
        avg_cv_score, std_cv_score = train(X, Y, batch_size, optimizer_params, es_params, rlr_params, width, depth, folds)
        mlflow.log_params(dict(error=False))
    except Exception as e:
        print(e)
        mlflow.log_params(
            dict(error=True)
        )

        avg_cv_score = 99
        std_cv_score = 99

    mlflow.log_params(optimizer_params)
    mlflow.log_params(es_params)
    mlflow.log_params(dict(
        batch_size=batch_size,
        width=width,
        depth=depth,
        folds=folds
    ))

    mlflow.log_metric("CV Average", avg_cv_score)
    mlflow.log_metric("CV Std", std_cv_score)
    mlflow.end_run()