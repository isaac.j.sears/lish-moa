import numpy as np

from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint
from sklearn.model_selection import KFold
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization, Input
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import regularizers

from lishmoautil.DataUtil import extract_x, extract_y, predict_avg_ensemble, my_log_loss
from lishmoautil.Globals import *

from tensorflow.python.platform import tf_logging as logging


class ReduceLRBacktrack(ReduceLROnPlateau):
    def __init__(self, best_path, *args, **kwargs):
        super(ReduceLRBacktrack, self).__init__(*args, **kwargs)
        self.best_path = best_path

    def on_epoch_end(self, epoch, logs=None):
        current = logs.get(self.monitor)
        if current is None:
            logging.warning('Reduce LR on plateau conditioned on metric `%s` '
                            'which is not available. Available metrics are: %s',
                             self.monitor, ','.join(list(logs.keys())))
        if not self.monitor_op(current, self.best): # not new best
            if not self.in_cooldown(): # and we're not in cooldown
                if self.wait+1 >= self.patience: # going to reduce lr
                    # load best model so far
                    print("\nBacktracking to best model before reducting LR")
                    self.model.load_weights(self.best_path)

        super().on_epoch_end(epoch, logs) # actually reduce LR


def get_multilabel_dnn(X, Y):
    model = Sequential()
    model.add(BatchNormalization(input_dim=np.shape(X)[1]))
    model.add(Dense(512, activation='elu'))

    for idx in range(0, 11):
        model.add(BatchNormalization())
        model.add(Dense(256, activation='elu'))
        model.add(BatchNormalization())

    model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

    adm = Adam()
    model.compile(loss='binary_crossentropy', optimizer=adm)

    return model


X = extract_x(f'{DATA_ROOT}/train_features.csv')
Y_scored = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

kfold = KFold(n_splits=50, shuffle=True)
scored_tgt_predictors = list()
all_scores = list()
histories = list()
epochs = 10000

for idx, (train, test) in enumerate(kfold.split(X, Y_scored)):
    scored_tgt_predictors.append(get_multilabel_dnn(X, Y_scored))

    scored_tgt_predictors[idx].fit(
        X[train],
        Y_scored[train],
        epochs=epochs,
        batch_size=128,
        verbose=1,
        validation_data=(X[test], Y_scored[test]),
        callbacks=[
            EarlyStopping(patience=3, min_delta=1e-5, restore_best_weights=True),
            ModelCheckpoint('/tmp/backtrack.h5', save_best_only=True, monitor='val_loss'),
            ReduceLRBacktrack(
                patience=1,
                factor=0.10,
                verbose=1,
                mode="auto",
                monitor='val_loss',
                min_delta=1e-9,
                best_path='/tmp/backtrack.h5'
            )
        ]
    )



    y_hat = scored_tgt_predictors[idx].predict(X[test])
    score = my_log_loss(Y_scored[test], y_hat)
    all_scores.append(score)
    print(f'[+] Done with fold {idx}, final validation score: {score}')


print(f'[+] CV complete, avg score: {np.average(all_scores)}')

for idx, m in enumerate(scored_tgt_predictors):
    print(f'Saving CV model {idx}')
    m.save(f'{MODEL_ROOT}/../vanilla-dnn-final/model-{idx}.h5')