import numpy as np

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD, Adam
from lishmoautil.DataUtil import extract_x, extract_y, my_log_loss, predict_avg_ensemble
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.models import load_model
from sklearn.metrics import log_loss
from sklearn.model_selection import KFold
from lishmoautil.Globals import *


def get_multilabel_dnn(X, Y):
    model = Sequential()
    model.add(BatchNormalization(input_dim=np.shape(X)[1]))
    model.add(Dense(512, activation='elu'))

    for idx in range(0, 11):
        model.add(BatchNormalization())
        model.add(Dense(256, activation='elu'))
        model.add(BatchNormalization())

    model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

    adm = Adam()
    model.compile(loss='binary_crossentropy', optimizer=adm)

    return model


def train_nonscored(X, Y_nonscored):
    kfold = KFold(n_splits=50, shuffle=True, random_state=589)
    nonscored_predictors = list()
    nonscored_scores = list()
    for idx, (train, test) in enumerate(kfold.split(X, Y_nonscored)):
        nonscored_predictors.append(get_multilabel_dnn(X, Y_nonscored))

        history = nonscored_predictors[idx].fit(
            X[train],
            Y_nonscored[train],
            epochs=50,
            batch_size=128,
            verbose=1,
            validation_data=(X[test], Y_nonscored[test]),
            callbacks=[
                EarlyStopping(patience=5, min_delta=1e-6, restore_best_weights=True),
                ReduceLROnPlateau(patience=2, factor=0.5, verbose=1, mode="auto")
            ]
        )

        y_hat = nonscored_predictors[idx].predict(X[test])
        score = my_log_loss(Y_nonscored[test], y_hat)
        nonscored_scores.append(score)

        print(f'[+] Done with fold {idx}, final validation score: {score}')

    print(f'[+] Cross validation complete, CV scores:')
    print(nonscored_scores)
    print(f'\t Average: {sum(nonscored_scores) / len(nonscored_scores)}')
    #Y_nonscored_hat = predict_avg_ensemble(nonscored_predictors, X)

    print('[*] Saving models...')
    for idx, model in enumerate(nonscored_predictors):
        model.save(f'{MODEL_ROOT}/../nonscored-predictor-final/model-{idx}.h5')


def train_meta(X, Y, Y_nonscored):
    X_augmented = np.concatenate((X, Y_nonscored), axis=1)
    kfold = KFold(n_splits=5, shuffle=True, random_state=589)
    metamodels = list()
    all_scores = list()
    for idx, (train, test) in enumerate(kfold.split(X_augmented, Y)):
        metamodels.append(get_multilabel_dnn(X_augmented, Y))

        history = metamodels[idx].fit(
            X_augmented[train],
            Y[train],
            epochs=50,
            batch_size=128,
            verbose=1,
            validation_data=(X_augmented[test], Y[test]),
            callbacks=[
                EarlyStopping(patience=5, min_delta=1e-6, restore_best_weights=True),
                ReduceLROnPlateau(patience=2, factor=0.5, verbose=1, mode="auto")
            ]
        )

        y_hat = metamodels[idx].predict(X_augmented[test])
        score = my_log_loss(Y[test], y_hat)
        all_scores.append(score)

        print(f'[+] Done with fold {idx}, final validation score: {score}')

        rval_score = my_log_loss(Y[test], (y_hat > 0.1))
        print(f'[*] Just-for-fun boost-rounded validation score: {rval_score}')

    print(f'[+] Cross validation complete, CV scores:')
    print(all_scores)
    print(f'\t Average: {sum(all_scores) / len(all_scores)}')




if __name__ == '__main__':
    X = extract_x(f'{DATA_ROOT}/train_features.csv')
    Y = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')
    Y_nonscored = extract_y(f'{DATA_ROOT}/train_targets_nonscored.csv')
    assert np.shape(X)[0] == np.shape(Y)[0]

    train_nonscored(X, Y_nonscored)

    # nonscored_ensemble = [load_model(f'{MODEL_ROOT}/../nonscored-predictor-final/model-{idx}.h5') for idx in range(0, 10)]
    # Y_nonscored_hat = predict_avg_ensemble(nonscored_ensemble, X)
    # sanity_check = my_log_loss(Y_nonscored, Y_nonscored_hat)
    # print(f'[*] Y_nonscored sanity check: {sanity_check}')
    #
    # Y_nonscored_hat_rounded = np.rint(Y_nonscored_hat)
    # Y_nonscored_hat_boost_rounded = Y_nonscored_hat > 0.2
    # train_meta(X, Y, Y_nonscored_hat_boost_rounded)