import numpy as np

from lishmoautil.Globals import *
from lishmoautil.MultilabelDNN import MultilabelDNN
from sklearn.model_selection import KFold


def getCVPreds(X):
    model_paths = [f'{MODEL_ROOT}/scored-multilabel-dnn/model-{idx}.h5' for idx in range(0, 10)]
    curr_model = MultilabelDNN(name='multilabel-dnn', pretrained_models=model_paths)
    all_preds = curr_model.predict_raw(X)

    # TODO: Verify that this is resulting in an appropriate-sized Ycv
    res = np.concatenate(all_preds, axis=1)
    print(np.shape(res))

    return res


def train(params):
    X = np.load(f'{CACHE_PATH}/X.npy', allow_pickle=False)
    Y = np.load(f'{CACHE_PATH}/Y.npy', allow_pickle=False)


    assert np.shape(X)[0] == np.shape(Y)[0]

    # print("[+] Extraction complete")
    # print(f'X shape: {np.shape(X)}')
    # print(f'\t{np.shape(X)[0]} training examples with {np.shape(X)[1]} features')
    # print(f'Y shape: {np.shape(Y)}')
    # print(f'\t{np.shape(Y)[0]} training examples with {np.shape(Y)[1]} possible labels')
    #
    # print("[*] Running k-fold cross validation")
    X_augmented = np.concatenate((X, Y_nonscored_hat), axis=1)
    kfold = KFold(n_splits=5, shuffle=True)

    cv_scores = list()
    train_scores = list()

    for idx, (train, test) in enumerate(kfold.split(X, Y)):
        Y_cv = getCVPreds(X[train])

        np.concatenate()


if __name__ == '__main__':
    train({})