import numpy as np

from lishmoautil.DataUtil import extract_x, extract_y
from lishmoautil import Globals


def _save_cache(X, Y, pathx, pathy):
    assert np.shape(X)[0] == np.shape(Y)[0]

    print("[*] Saving data...")
    np.save(pathx, X, allow_pickle=False)
    np.save(pathy, Y, allow_pickle=False)

    print("[*] Self-checking..")
    X_saved = np.load(f'{pathx}.npy', allow_pickle=False)
    Y_saved = np.load(f'{pathy}.npy', allow_pickle=False)

    assert (X_saved == X).all()
    assert (Y_saved == Y).all()

    print("[+] Cache and self-check completed successfully")


def build_cache(data_path: str, cache_path: str):
    X = extract_x(f'{data_path}/train_features.csv')
    Y = extract_y(f'{data_path}/train_targets_scored.csv')
    _save_cache(X, Y, f'{cache_path}/X', f'{cache_path}/Y')

    Y_nonscored = extract_y(f'{data_path}/train_targets_nonscored.csv')
    _save_cache(X, Y_nonscored, f'{cache_path}/X', f'{cache_path}/Y_nonscored')


if __name__ == '__main__':
    build_cache(f'{Globals.DATA_ROOT}', f'{Globals.CACHE_PATH}')
