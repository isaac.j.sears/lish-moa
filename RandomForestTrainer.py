
from lishmoautil.DataUtil import *
from lishmoautil.Globals import *
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold


def train(X, Y, folds):
    all_scores = list()

    for seed in [67, 982, 2450, 2, 34311]:  # Hard-coded test seeds for reproducibility
        kfold = KFold(n_splits=folds, shuffle=True, random_state=seed)
        fold_scores = list()

        for idx, (train, test) in enumerate(kfold.split(X, Y)):
            curr_model = RandomForestClassifier(n_jobs=-1)
            curr_model.fit(X[train], Y[train])

            Y_hat = curr_model.predict(X[test])
            oof_score = my_log_loss(Y[test], Y_hat)

            Y_hat = curr_model.predict(X[train])
            train_score = my_log_loss(Y[train], Y_hat)

            print(f'[+] Finished fold {idx}, oof score: {oof_score}, train score: {train_score}')
            fold_scores.append(oof_score)

        print(f'[+] Done with seed {seed}, avg CV: {sum(fold_scores) / len(fold_scores)}')
        all_scores += fold_scores


    avg_all = sum(all_scores) / len(all_scores)
    std_all = np.std(np.array(all_scores))
    print(f'[+] 5-seed CV complete, avg CV: {avg_all}, std CV: {std_all}')

    return avg_all, std_all


if __name__ == '__main__':
    X = extract_x(f'{DATA_ROOT}/train_features.csv')
    Y = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

    train(X, Y, 10)