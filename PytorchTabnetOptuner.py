import optuna

import numpy as np
import torch
import torch.nn.functional as F
from pytorch_tabnet.tab_model import TabNetRegressor
from lishmoautil.ModelUtil import AbstractTrainableModel, cross_validate
from lishmoautil.TabNetMetric import CustomMetric

from typing import Callable


class TabnetModel(AbstractTrainableModel):
    def __init__(self, fit_params: dict, model_factory: Callable):
        self.fit_params = fit_params
        self.__model_factory = model_factory
        self.models = list()
        self.models.append(model_factory())

    def fit(self, X_train, Y_train, X_valid, Y_valid):
        self.models = list()  # TODO: clearing models on every iteration to avoid memory exhaustion
        self.models.append(self.__model_factory())
        return self.models[-1].fit(
            X_train=X_train, y_train=Y_train,
            eval_set=[(X_valid, Y_valid)],
            eval_metric=[CustomMetric],
            loss_fn=F.binary_cross_entropy_with_logits,
            max_epochs=1000,
            **self.fit_params
        )

    def predict(self, X):  # tabnet returns logits
        pred_raw = self.models[-1].predict(X)
        return  1 / (1 + np.exp(-pred_raw))


def objective(trial):
    batch_size = trial.suggest_int('batch_size', 128, 4096)
    es_patience = trial.suggest_int('es_patience', 5, 25)

    fit_params = dict(
        batch_size=batch_size,
        patience=es_patience
    )

    n_d = trial.suggest_int('n_d', 4, 100)
    n_a = n_d

    cat_idxs = trial.suggest_categorical('cat_idxs', [[0, 2], []])
    if len(cat_idxs) == 0:
        cat_dims = []
    else:
        cat_dims = [2, 2]


    scheduler_fn_name = trial.suggest_categorical('scheduler_fn', ['rlr', None])

    if scheduler_fn_name == 'rlr':
        scheduler_params=dict(
            mode="min",
            patience=trial.suggest_int('rlr_patience', 1, es_patience - 1),
            min_lr=1e-7,
            factor=trial.suggest_uniform('rlr_factor', 0.01, 0.99)
        )

        scheduler_fn = torch.optim.lr_scheduler.ReduceLROnPlateau

    else:
        scheduler_params = None
        scheduler_fn = None

    build_params = dict(
        n_d=n_d,
        n_a=n_a,
        n_steps=trial.suggest_int('n_steps', 1, 15),
        gamma=trial.suggest_uniform('gamma', 1.0, 2.0),
        cat_idxs=cat_idxs,
        cat_dims=cat_dims,
        n_independent=trial.suggest_int('n_independent', 1, 7),
        n_shared=trial.suggest_int('n_shared', 1, 7),
        momentum=trial.suggest_uniform('momentum', 0.005, 0.5),
        lambda_sparse=trial.suggest_categorical('lambda_sparse', [1e-4, 1e-3, 1e-2]),
        optimizer_fn=torch.optim.Adam,
        optimizer_params={
            'lr': trial.suggest_loguniform('learning_rate', 1e-7, 1e-1)
        },
        scheduler_fn=scheduler_fn,
        scheduler_params=scheduler_params,
        verbose=0
    )

    model_factory = lambda: TabNetRegressor(**build_params)

    stm = TabnetModel(fit_params=fit_params, model_factory=model_factory)
    score = cross_validate(model=stm, folds=5, seeds=[1337])

    return score



if __name__ == '__main__':
    study = optuna.create_study(
        study_name='Tabnet',
        storage='mysql://root:root@localhost/optuna',
        load_if_exists=True
    )

    study.optimize(objective, n_trials=500, n_jobs=3)