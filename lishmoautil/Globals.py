import os
import subprocess

if ('HOSTNAME' in os.environ and os.environ['HOSTNAME'] == 'Hera') or \
        ('CI_RUNNER_DESCRIPTION' in os.environ and os.environ['CI_RUNNER_DESCRIPTION'] == 'Hera-lish-moa'):
    DATA_ROOT = '/zfs-data-pool/kaggle/lish-moa'
    CACHE_PATH = '/zfs-data-pool/kaggle/lish-moa/cache'
    LOGS_PATH = '/zfs-data-pool/kaggle/lish-moa/logs'
    MODEL_ROOT = '/zfs-data-pool/kaggle/lish-moa/models/deployable-set'
    KAGGLE = False
elif 'USER' in os.environ and os.environ['USER'] == 'isears':
    DATA_ROOT = './testdata'
    CACHE_PATH = './testdata/cache'
    LOGS_PATH = './logs'
    MODEL_ROOT = './models'
    KAGGLE = False
else:  # Must be kaggle
    DATA_ROOT = '/kaggle/input/lish-moa'
    CACHE_PATH = '/kaggle/input/cache'
    LOGS_PATH = './logs'
    MODEL_ROOT = '/kaggle/input/moa-prediction-models'
    KAGGLE = True