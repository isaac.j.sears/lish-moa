from lishmoautil.TabNetWrapper import TabNetWrapper

import numpy as np


def tabnetPredict(model_paths, X):
    print('[*] Loading models:')
    print(model_paths)

    cv_models = list()
    for p in model_paths:
        curr_model = TabNetWrapper()
        curr_model.load_model(p)
        cv_models.append(curr_model)

    y_hat_all = [m.predict(X) for m in cv_models]
    y_hat = np.mean(y_hat_all, axis=0)

    return y_hat