from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from sklearn.model_selection import KFold
from datetime import datetime

import numpy as np
import tensorflow as tf
import os
import mlflow

from lishmoautil import Globals

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class MultilabelDNN:
    def __init__(self, name: str, pretrained_models: list = None, params: dict = None):
        self.name = name
        self.cv_models = pretrained_models

        if 'TRAIN' in os.environ and os.environ['TRAIN'] == 'true':
            self.mlflow = True
        else:
            self.mlflow = False

        if params is None:
            # Anything that should be logged by mlflow
            self.params = {
                'depth': 5,
                'epochs': 50,
                'batch_size': 128,
                'dropout': 0.2,
                'learning_rate': 1e-4,
                'early_stop_cb': EarlyStopping(patience=3, min_delta=1e-5, restore_best_weights=True),
                'reduce_lr_cb': ReduceLROnPlateau(patience=1, factor=0.5, verbose=1, mode="auto"),
                'validation_splits': 10
            }
        else:
            self.params = params


    def _get_model(self, X, Y):
        model = Sequential()
        model.add(BatchNormalization(input_dim=np.shape(X)[1]))
        model.add(Dense(512, activation='elu'))

        for idx in range(0, self.params['depth'] - 1):
            #model.add(Dropout(self.dropout))
            model.add(BatchNormalization())
            model.add(Dense(256, activation='elu'))
            model.add(BatchNormalization())

        model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

        sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        adm = Adam()
        model.compile(loss='binary_crossentropy', optimizer=adm)

        return model


    def setup_mlflow(self, experiment_id, tracking_uri):
        mlflow.set_tracking_uri(tracking_uri)
        mlflow.set_experiment(experiment_id)
        self.mlflow_run = mlflow.start_run()

        mlflow.log_params(self.params)

    def cross_validate(self, X, Y):
        if self.mlflow:
            print('[*] Starting mlflow run...')
            self.setup_mlflow('lish-moa', 'http://192.168.45.105:5000')

        self.cv_models = list()  # Clear any models in case validate gets called twice for some reason
        assert np.shape(X)[0] == np.shape(Y)[0]

        print("[+] Extraction complete")
        print(f'X shape: {np.shape(X)}')
        print(f'\t{np.shape(X)[0]} training examples with {np.shape(X)[1]} features')
        print(f'Y shape: {np.shape(Y)}')
        print(f'\t{np.shape(Y)[0]} training examples with {np.shape(Y)[1]} possible labels')

        print("[*] Running k-fold cross validation")
        kfold = KFold(n_splits=self.params['validation_splits'], shuffle=True)
        #kfold = MultilabelStratifiedKFold(n_splits=self.validation_splits, shuffle=True)
        fold_idx = 0
        all_scores = list()

        start_time = datetime.now().strftime("%Y%m%d-%H%M%S")
        epoch_stop_points = list()

        for train, test in kfold.split(X, Y):
            curr_model = self._get_model(X, Y)

            tb_callback = tf.keras.callbacks.TensorBoard(
                log_dir=f'{Globals.LOGS_PATH}/{self.name}/{start_time}-cv{fold_idx}',
                histogram_freq=1
            )

            history = curr_model.fit(
                X[train],
                Y[train],
                epochs=self.params['epochs'],
                batch_size=self.params['batch_size'],
                verbose=0,
                validation_data=(X[test], Y[test]),
                callbacks=[
                    tb_callback,
                    self.params['early_stop_cb'],
                    self.params['reduce_lr_cb']
                ]
            )

            score = curr_model.evaluate(X[test], Y[test])
            epoch_stop_points.append(len(history.epoch))
            print(f'Score for fold {fold_idx}: {score}')

            if not os.path.exists(f'{Globals.MODEL_ROOT}/{self.name}'):
                os.makedirs(f'{Globals.MODEL_ROOT}/{self.name}')

            curr_model.save(f'{Globals.MODEL_ROOT}/{self.name}/model-{fold_idx}.h5')
            self.cv_models.append(curr_model)
            all_scores.append(score)

            fold_idx += 1

        avg_cv = sum(all_scores) / len(all_scores)
        print(f'[+] Completed CV, average score: {avg_cv}')

        if self.mlflow:
            print('[*] Ending mlflow run...')
            mlflow.log_metric("CV", avg_cv)
            mlflow.end_run()

        # This is tricky because we don't know where the stopping point is without access to validation data
        # avg_stop_pt = int(np.rint(np.mean(epoch_stop_points)))
        # print(f'[*] Training model on all data with {avg_stop_pt} epochs...')
        #
        #
        # full_data_model = self._get_model(X, Y)
        # full_data_model.fit(X, Y, batch_size=self.batch_size, epochs=avg_stop_pt, verbose=0, callbacks=[self.reduce_lr_cb])
        #
        # full_data_model.save(f'{Globals.MODEL_ROOT}/full-data-model.h5')

    def cv_ensemble_predict(self, X_test):
        predictions = self.predict_raw(X_test)

        y_hat_averaged = np.mean(predictions, axis=0)
        assert np.shape(y_hat_averaged)[0] == np.shape(X_test)[0], "[-] Prediction matrix dimensional mismatch"

        return y_hat_averaged

    def predict_raw(self, X_test):
        predictions = list()
        for model in self.cv_models:
            y_hat = model.predict(X_test)
            assert np.shape(y_hat)[0] == np.shape(X_test)[0], "[-] Prediction matrix dimensional mismatch"
            predictions.append(np.copy(y_hat))

        return predictions