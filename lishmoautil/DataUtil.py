import pandas as pd
import numpy as np

from scipy.stats import zscore
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold

# Non Z-scored version
# def extract_x(path):
#     df = pd.read_csv(path)
#     df.sort_values('sig_id', inplace=True)  # Always sort based on sig_id by convention for this project
#     # String-to-values map
#     replace_maps = {
#         'cp_type': {'trt_cp': 1, 'ctl_vehicle': 0},
#         'cp_dose': {'D1': 0, 'D2': 1}
#     }
#
#     df.replace(replace_maps, inplace=True)
#
#     return df.drop('sig_id', axis=1).to_numpy(dtype='float', copy=True)


def my_log_loss(y_true, y_pred):
    y_pred = np.clip(y_pred, 1e-15, 1 - 1e-15)
    aux = (1 - y_true) * np.log(1 - y_pred + 1e-15) + y_true * np.log(y_pred + 1e-15)
    return np.mean(-aux)


# TODO: Maybe replace the hard 1s and 0s with min/max's like in the loss function? Or just use the loss function for training?
def extract_y(data_ptr):

    if isinstance(data_ptr, str):  # String input, assume is file path
        df = pd.read_csv(data_ptr)
        dropped_cols = ['sig_id']
    elif isinstance(data_ptr, pd.DataFrame): # Dataframe input, assume is pre-stratified dataframe
        df = data_ptr
        dropped_cols = ['sig_id', 'drug_id', 'fold']
    else:
        assert False, "[-] Input to extract_y(data_ptr) must be either a file path (str) or a dataframe (pd.DataFrame)"

    df = df.sort_values('sig_id')
    return df.drop(dropped_cols, axis=1, errors='ignore').to_numpy(dtype='float', copy=True)


# Z-scored X extraction
# This was originally written as a simple file-to-np-array function, but ended up handling a lot of preprocessing
# In the future, these two functions should be decoupled
def extract_x(data_ptr):
    if isinstance(data_ptr, str):  # String input, assume is file path
        df = pd.read_csv(data_ptr)
    elif isinstance(data_ptr, pd.DataFrame):  # Dataframe input, assume is pre-stratified dataframe
        df = data_ptr
    else:
        assert False, "[-] Input to extract_y(data_ptr) must be either a file path (str) or a dataframe (pd.DataFrame)"

    df = df.sort_values('sig_id')  # Always sort based on sig_id by convention for this project

    # String-to-values map (also manually normalize cp_time)
    replace_maps = {
        'cp_type': {'trt_cp': 1, 'ctl_vehicle': 0},
        'cp_dose': {'D1': 0, 'D2': 1},
        'cp_time': {24: 1, 48: 2, 72: 3}
    }

    df.replace(replace_maps, inplace=True)

    # Drop metadata columns (really just sig_id now)
    gene_expression_cols = [col for col in df if col.startswith('g-')]
    cell_viability_cols = [col for col in df if col.startswith('c-')]
    original_cols = df.columns
    kept_cols = gene_expression_cols + cell_viability_cols + ['cp_type', 'cp_dose', 'cp_time']
    df = df.drop([col for col in original_cols if col not in kept_cols], axis=1)

    # Z-score gene expression and cell viability separately
    # axis-0 normalization
    df[gene_expression_cols] = df[gene_expression_cols].apply(zscore, axis=0)
    df[cell_viability_cols] = df[cell_viability_cols].apply(zscore, axis=0)

    return df.to_numpy(dtype='float', copy=True)


def extract_x_pca(train_path, test_path):
    train_df = pd.read_csv(train_path)
    train_df['set'] = 'train'
    test_df = pd.read_csv(test_path)
    test_df['set'] = 'test'
    all_df = pd.concat([train_df, test_df])

    gene_cols = [col for col in all_df.columns if col.startswith('g-')]
    cell_cols = [col for col in all_df.columns if col.startswith('c-')]

    # gene pca
    gene_components = 600
    g_pca = PCA(n_components=gene_components).fit_transform(all_df[gene_cols])
    all_df = all_df.join(pd.DataFrame(g_pca, columns=[f'g-pca-{idx}' for idx in range(0, np.shape(g_pca)[1])]))

    # cell pca
    cell_components = 50
    c_pca = PCA(n_components=cell_components).fit_transform(all_df[cell_cols])
    all_df = all_df.join(pd.DataFrame(c_pca, columns=[f'c-pca-{idx}' for idx in range(0, np.shape(c_pca)[1])]))

    variance_testable_cols = [col for col in all_df.columns if col.startswith('g-') or col.startswith('c-')]
    variance_nontestable_cols = [col for col in all_df.columns if not col.startswith('g-') and not col.startswith('c-')]
    significant_cols = VarianceThreshold(0.8).fit(all_df[variance_testable_cols]).get_support()
    all_df_vt_sig = all_df[variance_testable_cols].loc[:, significant_cols]

    all_df = all_df[variance_nontestable_cols].join(all_df_vt_sig)

    train_final = extract_x(all_df[all_df['set'] == 'train'])
    test_final = extract_x(all_df[all_df['set'] == 'test'])

    # assert np.shape(train_final)[0] == train_df.shape[0]
    # assert np.shape(test_final)[0] == test_df.shape[0]

    return train_final, test_final



def predict_avg_ensemble(model_list, X):
    predictions = list()
    for model in model_list:
        y_hat = model.predict(X)
        assert np.shape(y_hat)[0] == np.shape(X)[0], "[-] Prediction matrix dimensional mismatch"
        predictions.append(np.copy(y_hat))

    y_hat_averaged = np.mean(predictions, axis=0)
    assert np.shape(y_hat_averaged)[0] == np.shape(X)[0], "[-] Prediction matrix dimensional mismatch"

    return y_hat_averaged


def predict_softmax_ensemble(model_list, X):
    predictions = list()
    for model in model_list:
        y_hat = model.predict(X)
        assert np.shape(y_hat)[0] == np.shape(X)[0], "[-] Prediction matrix dimensional mismatch"
        predictions.append(np.copy(y_hat))

    # TODO: get max if one of the predictions has p > 0.5



"""
Post-processing
"""
def smooth_labels(labels, factor=0.1):
    labels *= (1 - factor)
    labels += (factor / labels.shape[1])

    return labels


def write_submission(y_hat, train_csv, sample_format_csv, out_csv):
    sample_df = pd.read_csv(sample_format_csv)
    train_df = pd.read_csv(train_csv)

    sig_ids = train_df.sort_values('sig_id')[['sig_id']]
    # TODO: Maybe incorporate some kind of state-awareness by using a datautil object?
    assert len(sig_ids) == np.shape(y_hat)[0], "[-] Didn't get the right amount of sig ids from the train csv, maybe training data was loaded from a different csv?"

    data_only = pd.DataFrame(data=y_hat, columns=sample_df.columns[1:])
    combined = pd.merge(sig_ids, data_only, left_index=True, right_index=True)

    combined.to_csv(out_csv, index=False)


def write_submission_ctrl(y_hat, train_csv, sample_format_csv, out_csv):
    sample_df = pd.read_csv(sample_format_csv)
    train_df = pd.read_csv(train_csv)

    sig_ids = train_df.sort_values('sig_id')[['sig_id']]
    # TODO: Maybe incorporate some kind of state-awareness by using a datautil object?
    assert len(sig_ids) == np.shape(y_hat)[0], \
        "[-] Didn't get the right amount of sig ids from the train csv, maybe training data was loaded from a different csv?"

    data_only = pd.DataFrame(data=y_hat, columns=sample_df.columns[1:])
    combined = pd.merge(sig_ids, data_only, left_index=True, right_index=True)

    not_sid_col = [col for col in combined.columns if col != 'sig_id']
    combined.loc[train_df['cp_type'] == 'ctl_vehicle', not_sid_col] = 0.0

    combined.to_csv(out_csv, index=False)


if __name__ == '__main__':
    from lishmoautil.Globals import *
    #X = extract_x(f'{DATA_ROOT}/train_features.csv')
    X_train, X_test = extract_x_pca(f'{DATA_ROOT}/train_features.csv', f'{DATA_ROOT}/test_features.csv')