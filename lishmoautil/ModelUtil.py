import pandas as pd
import numpy as np

from iterstrat.ml_stratifiers import MultilabelStratifiedKFold
from sklearn.model_selection import KFold
from lishmoautil.Globals import *
from lishmoautil.DataUtil import extract_x, extract_y, my_log_loss

from typing import Callable
from abc import ABC, abstractmethod


class AbstractTrainableModel(ABC):

    @abstractmethod
    def fit(self, X_trian, Y_train, X_valid, Y_valid):
        pass

    @abstractmethod
    def predict(self, X):
        pass


class SimpleTfModel(AbstractTrainableModel):
    def __init__(self, fit_params: dict, model_factory: Callable):
        self.fit_params = fit_params
        self.__model_factory = model_factory
        self.models = list()
        self.models.append(model_factory())

    def fit(self, X_train, Y_train, X_valid, Y_valid):
        self.models.append(self.__model_factory())
        return self.models[-1].fit(X_train, Y_train, validation_data=(X_valid, Y_valid), **self.fit_params)

    def predict(self, X):
        return self.models[-1].predict(X)



def _generate_folds(folds, seed=1337):
    scored = pd.read_csv(f'{DATA_ROOT}/train_targets_scored.csv')
    drug = pd.read_csv(f'{DATA_ROOT}/train_drug.csv')
    targets = scored.columns[1:]  # Everything except sig_id
    scored = scored.merge(drug, on='sig_id', how='left')

    # LOCATE DRUGS
    vc = scored.drug_id.value_counts()
    vc1 = vc.loc[vc <= 18].index.sort_values()
    vc2 = vc.loc[vc > 18].index.sort_values()

    # STRATIFY DRUGS 18X OR LESS
    dct1 = {}
    dct2 = {}
    skf = MultilabelStratifiedKFold(n_splits=folds, shuffle=True, random_state=seed)
    tmp = scored.groupby('drug_id')[targets].mean().loc[vc1]
    for fold, (idxT, idxV) in enumerate(skf.split(tmp, tmp[targets])):
        dd = {k: fold for k in tmp.index[idxV].values}
        dct1.update(dd)

    # STRATIFY DRUGS MORE THAN 18X
    skf = MultilabelStratifiedKFold(n_splits=folds, shuffle=True, random_state=seed)
    tmp = scored.loc[scored.drug_id.isin(vc2)].reset_index(drop=True)
    for fold, (idxT, idxV) in enumerate(skf.split(tmp, tmp[targets])):
        dd = {k: fold for k in tmp.sig_id[idxV].values}
        dct2.update(dd)

    # ASSIGN FOLDS
    scored['fold'] = scored.drug_id.map(dct1)
    scored.loc[scored.fold.isna(), 'fold'] = scored.loc[scored.fold.isna(), 'sig_id'].map(dct2)
    scored.fold = scored.fold.astype('int8')

    return scored


def cross_validate(model: AbstractTrainableModel, folds: int, seeds=(1337, 284, 11, 8351, 334)):

    all_scores = list()

    for seed in seeds:

        Y_fold_labeled = _generate_folds(folds, seed)
        this_seed_scores = list()

        for fold_idx in range(0, folds):

            # Gather folds
            Y_train_df = Y_fold_labeled.loc[Y_fold_labeled['fold'] != fold_idx]
            Y_valid_df = Y_fold_labeled.loc[Y_fold_labeled['fold'] == fold_idx] # To compare with OOF preds

            X_all = pd.read_csv(f'{DATA_ROOT}/train_features.csv')
            X_train_df = X_all.loc[X_all['sig_id'].isin(Y_train_df['sig_id'])]
            X_valid_df = X_all.loc[X_all['sig_id'].isin(Y_valid_df['sig_id'])]

            # Run preprocessing pipeline
            Y_train = extract_y(Y_train_df)
            Y_valid = extract_y(Y_valid_df)

            X_train = extract_x(X_train_df)
            X_valid = extract_x(X_valid_df)

            model.fit(X_train, Y_train, X_valid, Y_valid)

            oof_predictions = model.predict(X_valid)
            score = my_log_loss(Y_valid, oof_predictions)
            this_seed_scores.append(score)

            print(f'[+] Completed CV fold {fold_idx} with score {score}')


        avg_seed_score = np.average(np.array(this_seed_scores))
        std_seed_score = np.std(np.array(this_seed_scores))

        print(f'[+] Completed CV for seed {seed} with average score {avg_seed_score} (std {std_seed_score})')
        all_scores += this_seed_scores

    final_avg_score = np.average(np.array(all_scores))
    final_std_score = np.std(np.array(all_scores))
    print('[+] Completed CV for all seeds. Final stats:')
    print(f'\t Score Avg: {final_avg_score}')
    print(f'\t Score Std: {final_std_score}')

    return final_avg_score

def cross_validate_nonscored(model: AbstractTrainableModel, nonscored_model: AbstractTrainableModel,
        folds: int, seeds=(1337, 284, 11, 8351, 334)):

    all_scores = list()

    for seed in seeds:

        Y_fold_labeled = _generate_folds(folds, seed)
        this_seed_scores = list()

        for fold_idx in range(0, folds):

            # Gather folds
            Y_train_df = Y_fold_labeled.loc[Y_fold_labeled['fold'] != fold_idx]
            Y_valid_df = Y_fold_labeled.loc[Y_fold_labeled['fold'] == fold_idx] # To compare with OOF preds

            X_all = pd.read_csv(f'{DATA_ROOT}/train_features.csv')
            X_train_df = X_all.loc[X_all['sig_id'].isin(Y_train_df['sig_id'])]
            X_valid_df = X_all.loc[X_all['sig_id'].isin(Y_valid_df['sig_id'])]

            Y_nonscored_all = pd.read_csv(f'{DATA_ROOT}/train_targets_nonscored.csv')
            Y_train_nonscored_df = Y_nonscored_all.loc[Y_nonscored_all['sig_id'].isin(Y_train_df['sig_id'])]
            Y_valid_nonscored_df = Y_nonscored_all.loc[Y_nonscored_all['sig_id'].isin(Y_valid_df['sig_id'])]

            # Run preprocessing pipeline
            Y_train = extract_y(Y_train_df)
            Y_valid = extract_y(Y_valid_df)

            Y_train_nonscored = extract_y(Y_train_nonscored_df)
            Y_valid_nonscored = extract_y(Y_valid_nonscored_df)

            X_train = extract_x(X_train_df)
            X_valid = extract_x(X_valid_df)

            # Fit nonscored predictor
            print('[*] Training nonscored predictor...')
            nonscored_model.fit(X_train, Y_train_nonscored, X_valid, Y_valid_nonscored)
            score = my_log_loss(Y_valid_nonscored, nonscored_model.predict(X_valid))
            print(f'[+] Nonscored predictor validation score: {score}')
            X_train_augmented = np.concatenate((X_train, nonscored_model.predict(X_train)), axis=1)
            X_valid_augmented = np.concatenate((X_valid, nonscored_model.predict(X_valid)), axis=1)


            # Fit metamodel
            print('[*] Training metamodel...')
            model.fit(X_train_augmented, Y_train, X_valid_augmented, Y_valid)

            oof_predictions = model.predict(X_valid_augmented)
            score = my_log_loss(Y_valid, oof_predictions)
            this_seed_scores.append(score)

            print(f'[+] Completed CV fold {fold_idx} with score {score}')


        avg_seed_score = np.average(np.array(this_seed_scores))
        std_seed_score = np.std(np.array(this_seed_scores))

        print(f'[+] Completed CV for seed {seed} with average score {avg_seed_score} (std {std_seed_score})')
        all_scores += this_seed_scores

    final_avg_score = np.average(np.array(all_scores))
    final_std_score = np.std(np.array(all_scores))
    print('[+] Completed CV for all seeds. Final stats:')
    print(f'\t Score Avg: {final_avg_score}')
    print(f'\t Score Std: {final_std_score}')

    return final_avg_score


def cross_validate_nonscored_pure(model: AbstractTrainableModel, folds: int, seeds=(1337, 284, 11, 8351, 334)):

    all_scores = list()

    for seed in seeds:

        Y_fold_labeled = _generate_folds(folds, seed)
        this_seed_scores = list()

        for fold_idx in range(0, folds):

            # Gather folds
            Y_train_df = Y_fold_labeled.loc[Y_fold_labeled['fold'] != fold_idx]
            Y_valid_df = Y_fold_labeled.loc[Y_fold_labeled['fold'] == fold_idx] # To compare with OOF preds

            X_all = pd.read_csv(f'{DATA_ROOT}/train_targets_nonscored.csv')
            X_train_df = X_all.loc[X_all['sig_id'].isin(Y_train_df['sig_id'])]
            X_valid_df = X_all.loc[X_all['sig_id'].isin(Y_valid_df['sig_id'])]

            # Run preprocessing pipeline
            Y_train = extract_y(Y_train_df)
            Y_valid = extract_y(Y_valid_df)

            X_train = extract_x(X_train_df)
            X_valid = extract_x(X_valid_df)

            model.fit(X_train, Y_train, X_valid, Y_valid)

            oof_predictions = model.predict(X_valid)
            score = my_log_loss(Y_valid, oof_predictions)
            this_seed_scores.append(score)

            print(f'[+] Completed CV fold {fold_idx} with score {score}')


        avg_seed_score = np.average(np.array(this_seed_scores))
        std_seed_score = np.std(np.array(this_seed_scores))

        print(f'[+] Completed CV for seed {seed} with average score {avg_seed_score} (std {std_seed_score})')
        all_scores += this_seed_scores

    final_avg_score = np.average(np.array(all_scores))
    final_std_score = np.std(np.array(all_scores))
    print('[+] Completed CV for all seeds. Final stats:')
    print(f'\t Score Avg: {final_avg_score}')
    print(f'\t Score Std: {final_std_score}')

    return final_avg_score


def cross_validate_unstratified(model: AbstractTrainableModel, folds: int, seeds=(1337, 284, 11, 8351, 334)):
    X = extract_x(f'{DATA_ROOT}/train_features.csv')
    Y = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

    all_scores = list()

    for seed in seeds:
        kfold = KFold(n_splits=folds, shuffle=True)

        this_seed_scores = list()

        for fold_idx, (train, test) in enumerate(kfold.split(X, Y)):
            model.fit(X[train], Y[train], X[test], Y[test])

            oof_predictions = model.predict(X[test])
            score = my_log_loss(Y[test], oof_predictions)

            this_seed_scores.append(score)
            print(f'[+] Completed CV fold {fold_idx} with score {score}')

        avg_seed_score = np.average(np.array(this_seed_scores))
        std_seed_score = np.std(np.array(this_seed_scores))

        print(f'[+] Completed CV for seed {seed} with average score {avg_seed_score} (std {std_seed_score})')
        all_scores += this_seed_scores

    final_avg_score = np.average(np.array(all_scores))
    final_std_score = np.std(np.array(all_scores))
    print('[+] Completed CV for all seeds. Final stats:')
    print(f'\t Score Avg: {final_avg_score}')
    print(f'\t Score Std: {final_std_score}')

    return final_avg_score


if __name__ == '__main__':
    from tensorflow.keras import Sequential
    from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization
    from tensorflow.keras.optimizers import SGD, Adam
    from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau

    X = extract_x(f'{DATA_ROOT}/train_features.csv')
    Y = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

    def get_model():
        model = Sequential()
        model.add(BatchNormalization(input_dim=np.shape(X)[1]))
        model.add(Dense(1024, activation='elu'))

        for idx in range(0, 5):
            model.add(BatchNormalization())
            model.add(Dense(1024, activation='elu'))
            model.add(BatchNormalization())

        model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

        adm = Adam()
        model.compile(loss='binary_crossentropy', optimizer=adm)
        return model

    fit_params = dict(
        epochs=500,
        batch_size=128,
        verbose=0,
        callbacks=[
            EarlyStopping(patience=9, min_delta=1e-5, restore_best_weights=True),
            ReduceLROnPlateau(patience=1, factor=0.9, verbose=0, mode="auto")
        ]
    )

    cross_validate(model=SimpleTfModel(fit_params, get_model), folds=5)
