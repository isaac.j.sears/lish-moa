import numpy as np

from pytorch_tabnet.metrics import Metric


class CustomMetric(Metric):
    def __init__(self):
        self._name = "customlogloss"
        self._maximize = False

    def __call__(self, y_true, y_pred):
        logits = 1 / (1 + np.exp(-y_pred))
        return CustomMetric.logloss(y_true, logits)

    @staticmethod
    def logloss(y_true, y_pred):
        aux = (1 - y_true) * np.log(1 - y_pred + 1e-15) + y_true * np.log(y_pred + 1e-15)
        return np.mean(-aux)
