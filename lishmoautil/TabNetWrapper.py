from pytorch_tabnet.tab_model import TabNetRegressor
import numpy as np


class TabNetWrapper(TabNetRegressor):
    def predict(self, X):
        raw = TabNetRegressor.predict(self, X)
        return 1 / (1 + np.exp(-raw))