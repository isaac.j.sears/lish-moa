import numpy as np


def evaluate(Y_actual, Y_hat):
    assert np.shape(Y_actual) == np.shape(Y_hat), f"[-] Size mismatch: {np.shape(Y_actual)} != {np.shape(Y_hat)}"
    M, N = np.shape(Y_actual) # M is number of targets, N is number of examples

    Y_hat = np.minimum(Y_hat, 1 - 1e-15)  # Replace all greater than 1 - 10^-15
    Y_hat = np.maximum(Y_hat, 1e-15)  # Replace all values less than 10^-15

    log_part = (Y_actual * np.log(Y_hat)) + ((1 - Y_actual) * np.log(1 - Y_hat))

    return np.sum((np.sum(log_part, axis=1) / N)) / (- M)

