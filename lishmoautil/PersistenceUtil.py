from pathlib import Path

def save_tabnet(m, path):
    """
    Basically stolen from:
    https://github.com/dreamquark-ai/tabnet/blob/855befc5a2cd153509b8c93eccdea866bf094a29/pytorch_tabnet/abstract_model.py#L273
    """
    saved_params = {}
    for key, val in m.get_params().items():
        if isinstance(val, type):
            # Don't save torch specific params
            continue
        else:
            saved_params[key] = val

    # Create folder
    Path(path).mkdir(parents=True, exist_ok=True)

    # Save models params
    with open(Path(path).joinpath("model_params.json"), "w", encoding="utf8") as f:
        json.dump(saved_params, f)

    # Save state_dict
    torch.save(self.network.state_dict(), Path(path).joinpath("network.pt"))
    shutil.make_archive(path, "zip", path)
    shutil.rmtree(path)
    print(f"Successfully saved model at {path}.zip")
    return f"{path}.zip"