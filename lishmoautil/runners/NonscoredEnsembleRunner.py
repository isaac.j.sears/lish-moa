import numpy as np

from tensorflow.keras.models import load_model

from lishmoautil.Globals import *
from lishmoautil.DataUtil import *


def predict(X, nonscored_predictors, ensemble_predictors):
    Y_nonscored_hat = predict_avg_ensemble(nonscored_predictors, X)
    X_augmented = np.concatenate((X, Y_nonscored_hat), axis=1)
    Y_hat = predict_avg_ensemble(ensemble_predictors, X_augmented)

    return Y_hat

def run():
    print('[*] Running sanity check...')
    X_train = extract_x(f'{DATA_ROOT}/train_features.csv')
    Y_train = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

    nonscored_predictors = [load_model(f'{MODEL_ROOT}/nonscored-multilabeldnn/model-{idx}.h5') for idx in range(0, 5)]
    ensemble_predictors = [load_model(f'{MODEL_ROOT}/nonscored-ensemble/model-{idx}.h5') for idx in range(0, 5)]

    Y_hat = predict(X_train, nonscored_predictors, ensemble_predictors)
    sanity_loss = my_log_loss(Y_train, Y_hat)
    print(f'[+] Sanity check resulted in {sanity_loss} loss')

    print('[*] Loading test data...')
    X = extract_x(f'{DATA_ROOT}/test_features.csv')
    print(f'\t{np.shape(X)[0]} examples with {np.shape(X)[1]} features')

    print('[*] Making predictions...')
    Y_hat = predict(X, nonscored_predictors, ensemble_predictors)

    # Smooth for regularization
    Y_hat = smooth_labels(Y_hat)

    print('[*] Writing submission...')
    write_submission_ctrl(Y_hat, f'{DATA_ROOT}/test_features.csv', f'{DATA_ROOT}/sample_submission.csv', './submission.csv')

    print("[+] Done")