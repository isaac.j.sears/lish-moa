import numpy as np

from lishmoautil.Globals import *
from lishmoautil.DataUtil import extract_x, extract_y, write_submission
from lishmoautil.TabNetMetric import CustomMetric
from lishmoautil import SimpleAvgEnsemble


def run():
    tabnet_model_paths = [f'{MODEL_ROOT}/tabnet-{idx}.zip' for idx in range(0, 10)]

    print('[*] Running sanity check...')
    X_train = extract_x(f'{DATA_ROOT}/train_features.csv')
    Y_train = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')
    y_hat = SimpleAvgEnsemble.tabnetPredict(tabnet_model_paths, X_train)
    sanity_loss = CustomMetric.logloss(Y_train, y_hat)
    print(f'[+] Sanity check resulted in {sanity_loss} loss')

    print('[*] Loading test data...')
    X = extract_x(f'{DATA_ROOT}/test_features.csv')
    print(f'\t{np.shape(X)[0]} examples with {np.shape(X)[1]} features')

    print('[*] Making predictions...')
    y_hat = SimpleAvgEnsemble.tabnetPredict(tabnet_model_paths, X)

    print('[*] Writing submission...')
    write_submission(y_hat, f'{DATA_ROOT}/test_features.csv', f'{DATA_ROOT}/sample_submission.csv', './submission.csv')

    print("[+] Done")