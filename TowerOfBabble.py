import numpy as np
import pandas as pd

from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint
from sklearn.model_selection import KFold
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization, Input, concatenate
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import regularizers
from tensorflow import keras

from lishmoautil.DataUtil import extract_x, extract_y, predict_avg_ensemble, my_log_loss
from lishmoautil.Globals import *

from tensorflow.python.platform import tf_logging as logging
from scipy.stats import zscore


class ReduceLRBacktrack(ReduceLROnPlateau):
    def __init__(self, best_path, *args, **kwargs):
        super(ReduceLRBacktrack, self).__init__(*args, **kwargs)
        self.best_path = best_path

    def on_epoch_end(self, epoch, logs=None):
        current = logs.get(self.monitor)
        if current is None:
            logging.warning('Reduce LR on plateau conditioned on metric `%s` '
                            'which is not available. Available metrics are: %s',
                             self.monitor, ','.join(list(logs.keys())))
        if not self.monitor_op(current, self.best): # not new best
            if not self.in_cooldown(): # and we're not in cooldown
                if self.wait+1 >= self.patience: # going to reduce lr
                    # load best model so far
                    print("\nBacktracking to best model before reducting LR")
                    self.model.load_weights(self.best_path)

        super().on_epoch_end(epoch, logs) # actually reduce LR


def extract_x_selective(data_ptr):
    if isinstance(data_ptr, str):  # String input, assume is file path
        df = pd.read_csv(data_ptr)
    elif isinstance(data_ptr, pd.DataFrame):  # Dataframe input, assume is pre-stratified dataframe
        df = data_ptr
    else:
        assert False, "[-] Input to extract_y(data_ptr) must be either a file path (str) or a dataframe (pd.DataFrame)"

    df = df.sort_values('sig_id')  # Always sort based on sig_id by convention for this project

    # String-to-values map (also manually normalize cp_time)
    replace_maps = {
        'cp_type': {'trt_cp': 1, 'ctl_vehicle': 0},
        'cp_dose': {'D1': 0, 'D2': 1},
        'cp_time': {24: 1, 48: 2, 72: 3}
    }

    df.replace(replace_maps, inplace=True)

    # Drop metadata columns (really just sig_id now)
    gene_expression_cols = [col for col in df if col.startswith('g-')]
    cell_viability_cols = [col for col in df if col.startswith('c-')]
    original_cols = df.columns
    kept_cols = gene_expression_cols + cell_viability_cols + ['cp_type', 'cp_dose', 'cp_time']
    #kept_cols = gene_expression_cols
    df = df.drop([col for col in original_cols if col not in kept_cols], axis=1)

    # Z-score gene expression and cell viability separately
    # axis-0 normalization
    df[gene_expression_cols] = df[gene_expression_cols].apply(zscore, axis=0)
    df[cell_viability_cols] = df[cell_viability_cols].apply(zscore, axis=0)

    gene_numpy = df[gene_expression_cols + ['cp_type', 'cp_dose', 'cp_time']].to_numpy(dtype='float', copy=True)
    cell_numpy = df[cell_viability_cols + ['cp_type', 'cp_dose', 'cp_time']].to_numpy(dtype='float', copy=True)

    return gene_numpy, cell_numpy


def get_multilabel_dnn(X, Y):
    model = Sequential()
    model.add(BatchNormalization(input_dim=np.shape(X)[1]))
    model.add(Dense(np.shape(X)[1], activation='elu'))

    for idx in range(0, 11):
        model.add(BatchNormalization())
        model.add(Dense(np.shape(X)[1], activation='elu'))
        model.add(BatchNormalization())

    model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

    adm = Adam()
    model.compile(loss='binary_crossentropy', optimizer=adm)

    return model


def tower_of_babel(X_gene, X_cell, Y):
    # genes
    gene_inputs = Input(shape=np.shape(X_gene)[1])
    gene_net = BatchNormalization()(gene_inputs)
    gene_net = Dense(np.shape(X_gene)[1], activation='relu')(gene_net)
    gene_net = BatchNormalization()(gene_net)
    gene_net = Dense(np.shape(X_gene)[1], activation='relu')(gene_net)
    gene_net = BatchNormalization()(gene_net)
    gene_net = Dense(np.shape(X_gene)[1], activation='relu')(gene_net)
    gene_net = BatchNormalization()(gene_net)
    gene_net = Dense(np.shape(Y)[1], activation='sigmoid')(gene_net)

    # cells
    cell_inputs = Input(shape=np.shape(X_cell)[1])
    cell_net = BatchNormalization()(cell_inputs)
    cell_net = Dense(np.shape(X_cell)[1], activation='relu')(cell_net)
    cell_net = BatchNormalization()(cell_net)
    cell_net = Dense(np.shape(X_cell)[1], activation='relu')(cell_net)
    cell_net = BatchNormalization()(cell_net)
    cell_net = Dense(np.shape(X_cell)[1], activation='relu')(cell_net)
    cell_net = BatchNormalization()(cell_net)
    cell_net = Dense(np.shape(Y)[1], activation='sigmoid')(cell_net)

    combined = concatenate([gene_net, cell_net])
    combined = Dropout(0.5)(combined)
    combined = Dense(np.shape(Y)[1], activation='relu')(combined)
    combined = Dropout(0.1)(combined)
    combined = Dense(np.shape(Y)[1], activation='sigmoid')(combined)

    model = keras.Model([gene_inputs, cell_inputs], combined)
    model.compile(loss='binary_crossentropy', optimizer=Adam())
    #model.summary()
    return model




X_gene, X_cell = extract_x_selective(f'{DATA_ROOT}/train_features.csv')
Y_scored = extract_y(f'{DATA_ROOT}/train_targets_scored.csv')

print(f'X_gene shape: {np.shape(X_gene)}')
print(f'X_cell shape: {np.shape(X_cell)}')
print(f'Y shape: {np.shape(Y_scored)}')



kfold = KFold(n_splits=5, shuffle=True, random_state=786)
scored_tgt_predictors = list()
all_scores = list()
histories = list()
epochs = 10000

for idx, (train, test) in enumerate(kfold.split(X_gene, Y_scored)):
    scored_tgt_predictors.append(tower_of_babel(X_gene, X_cell, Y_scored))

    scored_tgt_predictors[idx].fit(
        [X_gene[train], X_cell[train]],
        Y_scored[train],
        epochs=epochs,
        batch_size=64,
        verbose=1,
        validation_data=([X_gene[test], X_cell[test]], Y_scored[test]),
        callbacks=[
            EarlyStopping(patience=3, min_delta=1e-5, restore_best_weights=True),
            ModelCheckpoint('/tmp/backtrack.h5', save_best_only=True, monitor='val_loss'),
            ReduceLRBacktrack(
                patience=1,
                factor=0.10,
                verbose=1,
                mode="auto",
                monitor='val_loss',
                min_delta=1e-9,
                best_path='/tmp/backtrack.h5'
            )
        ]
    )



    y_hat = scored_tgt_predictors[idx].predict([X_gene[test], X_cell[test]])
    score = my_log_loss(Y_scored[test], y_hat)
    all_scores.append(score)
    print(f'[+] Done with fold {idx}, final validation score: {score}')


print(f'[+] CV complete, avg score: {np.average(all_scores)}')

for idx, m in enumerate(scored_tgt_predictors):
    print(f'Saving CV model {idx}')
    m.save(f'{MODEL_ROOT}/../vanilla-dnn-final/model-{idx}.h5')