import torch
import random
import mlflow

from PytorchTabnetTrainer import train


def optimize(param_space):

    while True: # Run forever
        mlflow.start_run()
        n_d = random.choice(param_space['n_d'])

        cat_idxs = random.choice(param_space['cat_idxs'])

        if len(cat_idxs) == 0:
            cat_dims = []
        else:
            cat_dims = [2, 2]

        curr_params = dict(
            n_d=n_d,
            n_a=n_d,
            n_steps=random.choice(param_space['n_steps']),
            gamma=random.choice(param_space['gamma']),
            n_independent=random.choice(param_space['n_independent']),
            momentum=random.choice(param_space['momentum']),
            lambda_sparse=random.choice(param_space['lambda_sparse']),
            optimizer_fn=torch.optim.Adam,
            optimizer_params={
                "lr": random.choice(param_space['optimizer_params']['lr'])
            },
            mask_type=random.choice(param_space['mask_type']),
            scheduler_params={
                "mode": "min",
                "patience": 5,
                "min_lr": 1e-7,
                "factor": random.choice(param_space['scheduler_params']['factor'])
            },
            scheduler_fn=torch.optim.lr_scheduler.ReduceLROnPlateau,
            verbose=0,
            cat_idxs=cat_idxs,
            cat_dims=cat_dims
        )

        mlflow.log_params(curr_params)
        cv_score, train_score = train(curr_params)
        mlflow.log_metric("CV Score", cv_score)
        mlflow.log_metric("Train Score", train_score)
        mlflow.log_metric("CV/Train Diff", cv_score - train_score)

        mlflow.end_run()


if __name__ == '__main__':
    mlflow.set_tracking_uri("http://192.168.45.105:5000")
    mlflow.set_experiment("lish-moa-tabnet-optimizing")

    param_space = dict(
        n_d=[x for x in range(8, 65)],
        #n_a=n_d, # TODO: fiddle with this manually later
        n_steps=[x for x in range(3, 11)],
        gamma=[1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0],
        n_independent=[1, 2, 3, 4, 5],
        momentum=[0.01 * x for x in range(0, 40)],
        lambda_sparse=[1e-7, 5e-7, 1e-6, 5e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2, 5e-2, 1e-1, 0.5],
        #optimizer_fn=torch.optim.Adam,
        optimizer_params={
            "lr": [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 0.2, 0.5, 0.7],
            #"weight_decay": 1e-5
        },
        mask_type=['sparsemax', 'entmax'],  # Default sparsemax
        scheduler_params={
            "mode": "min",
            "patience": 10,
            "min_lr": 1e-7,
            "factor": [0.001, 0.01, 0.1, 0.2, 0.5, 0.8, 0.9]
        },
        scheduler_fn=torch.optim.lr_scheduler.ReduceLROnPlateau,
        verbose=0,
        cat_idxs=[[0, 2], []],
        cat_dims=[[2, 2], []]
    )

    optimize(param_space)