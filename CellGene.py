import numpy as np
import pandas as pd
import json

from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint
from sklearn.model_selection import KFold
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization, Input, concatenate
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import regularizers
from tensorflow import keras

from lishmoautil.DataUtil import extract_x, extract_y, predict_avg_ensemble, my_log_loss
from lishmoautil.Globals import *

from tensorflow.python.platform import tf_logging as logging
from scipy.stats import zscore


class ReduceLRBacktrack(ReduceLROnPlateau):
    def __init__(self, best_path, *args, **kwargs):
        super(ReduceLRBacktrack, self).__init__(*args, **kwargs)
        self.best_path = best_path

    def on_epoch_end(self, epoch, logs=None):
        current = logs.get(self.monitor)
        if current is None:
            logging.warning('Reduce LR on plateau conditioned on metric `%s` '
                            'which is not available. Available metrics are: %s',
                             self.monitor, ','.join(list(logs.keys())))
        if not self.monitor_op(current, self.best): # not new best
            if not self.in_cooldown(): # and we're not in cooldown
                if self.wait+1 >= self.patience: # going to reduce lr
                    # load best model so far
                    #print("\nBacktracking to best model before reducting LR")
                    self.model.load_weights(self.best_path)

        super().on_epoch_end(epoch, logs) # actually reduce LR


def extract_x_selective(data_ptr):
    if isinstance(data_ptr, str):  # String input, assume is file path
        df = pd.read_csv(data_ptr)
    elif isinstance(data_ptr, pd.DataFrame):  # Dataframe input, assume is pre-stratified dataframe
        df = data_ptr
    else:
        assert False, "[-] Input to extract_y(data_ptr) must be either a file path (str) or a dataframe (pd.DataFrame)"

    df = df.sort_values('sig_id')  # Always sort based on sig_id by convention for this project

    # String-to-values map (also manually normalize cp_time)
    replace_maps = {
        'cp_type': {'trt_cp': 1, 'ctl_vehicle': 0},
        'cp_dose': {'D1': 0, 'D2': 1},
        'cp_time': {24: 1, 48: 2, 72: 3}
    }

    df.replace(replace_maps, inplace=True)

    # Drop metadata columns (really just sig_id now)
    gene_expression_cols = [col for col in df if col.startswith('g-')]
    cell_viability_cols = [col for col in df if col.startswith('c-')]
    original_cols = df.columns
    kept_cols = gene_expression_cols + cell_viability_cols + ['cp_type', 'cp_dose', 'cp_time']
    #kept_cols = gene_expression_cols
    df = df.drop([col for col in original_cols if col not in kept_cols], axis=1)

    # Z-score gene expression and cell viability separately
    # axis-0 normalization
    df[gene_expression_cols] = df[gene_expression_cols].apply(zscore, axis=0)
    df[cell_viability_cols] = df[cell_viability_cols].apply(zscore, axis=0)

    gene_numpy = df[gene_expression_cols + ['cp_type', 'cp_dose', 'cp_time']].to_numpy(dtype='float', copy=True)
    cell_numpy = df[cell_viability_cols + ['cp_type', 'cp_dose', 'cp_time']].to_numpy(dtype='float', copy=True)

    return gene_numpy, cell_numpy


def get_multilabel_dnn(X, Y):
    model = Sequential()
    model.add(BatchNormalization(input_dim=np.shape(X)[1]))
    model.add(Dense(np.shape(X)[1], activation='elu'))

    for idx in range(0, 11):
        model.add(BatchNormalization())
        model.add(Dense(np.shape(X)[1], activation='elu'))
        model.add(BatchNormalization())

    model.add(Dense(np.shape(Y)[1], activation='sigmoid'))

    adm = Adam()
    model.compile(loss='binary_crossentropy', optimizer=adm)

    return model


def extract_y_selective(data_ptr, kept_cols):
    df = pd.read_csv(data_ptr)
    # kept_cols = ['caspase_activator', 'bcl_inhibitor', 'apoptosis_stimulant', 'atpase_inhibitor', 'nfkb_inhibitor'
    #              'tubulin_inhibitor', 'cdk_inhibitor']
    #kept_cols = ['caspase_activator', 'apoptosis_stimulant']
    #kept_cols = ['apoptosis_stimulant']

    for c in kept_cols:
        assert c in df.columns

    dropped_cols = [c for c in df.columns if c not in kept_cols]
    #dropped_cols = ['sig_id']

    df = df.sort_values('sig_id')
    return df.drop(dropped_cols, axis=1, errors='ignore').to_numpy(dtype='float', copy=True)


# def get_top_columns(Y_true, Y_pred):
#
#     assert np.shape(Y_true) == np.shape(Y_pred)
#     top_10 = list()
#
#     for column_idx in range(0, np.shape(Y_true)[1]):

moa_df = pd.read_csv(f'{DATA_ROOT}/train_targets_scored.csv')
validation_by_moa = dict()

for idx, moa in enumerate(moa_df.columns[1:]):
    print('-' * 25)
    print(f'[*] Examining moa: {moa} ({idx} / {len(moa_df.columns)})')
    X_gene, X_cell = extract_x_selective(f'{DATA_ROOT}/train_features.csv')
    Y_scored = extract_y_selective(f'{DATA_ROOT}/train_targets_scored.csv', [moa])

    print(f'X_gene shape: {np.shape(X_gene)}')
    print(f'X_cell shape: {np.shape(X_cell)}')
    print(f'Y shape: {np.shape(Y_scored)}')



    kfold = KFold(n_splits=5, shuffle=True, random_state=786)
    gene_predictors = list()
    cell_predictors = list()

    cell_scores = list()
    gene_scores = list()
    histories = list()
    epochs = 10000

    for idx, (train, test) in enumerate(kfold.split(X_gene, Y_scored)):
        gene_predictors.append(get_multilabel_dnn(X_gene, Y_scored))
        cell_predictors.append(get_multilabel_dnn(X_cell, Y_scored))

        gene_predictors[idx].fit(
            X_gene[train],
            Y_scored[train],
            epochs=epochs,
            batch_size=256,
            verbose=0,
            validation_data=(X_gene[test], Y_scored[test]),
            callbacks=[
                EarlyStopping(patience=3, min_delta=1e-5, restore_best_weights=True),
                ModelCheckpoint('/tmp/backtrack.h5', save_best_only=True, monitor='val_loss'),
                ReduceLRBacktrack(
                    patience=1,
                    factor=0.10,
                    verbose=0,
                    mode="auto",
                    monitor='val_loss',
                    min_delta=1e-9,
                    best_path='/tmp/backtrack.h5'
                )
            ]
        )

        y_hat_gene = gene_predictors[idx].predict(X_gene[test])
        score = my_log_loss(Y_scored[test], y_hat_gene)
        gene_scores.append(score)
        print(f'[+] Done with fold {idx}, final gene validation score: {score}')

        cell_predictors[idx].fit(
            X_cell[train],
            Y_scored[train],
            epochs=epochs,
            batch_size=256,
            verbose=0,
            validation_data=(X_cell[test], Y_scored[test]),
            callbacks=[
                EarlyStopping(patience=3, min_delta=1e-5, restore_best_weights=True),
                ModelCheckpoint('/tmp/backtrack.h5', save_best_only=True, monitor='val_loss'),
                ReduceLRBacktrack(
                    patience=1,
                    factor=0.10,
                    verbose=0,
                    mode="auto",
                    monitor='val_loss',
                    min_delta=1e-9,
                    best_path='/tmp/backtrack.h5'
                )
            ]
        )

        y_hat_cell = cell_predictors[idx].predict(X_cell[test])
        score = my_log_loss(Y_scored[test], y_hat_cell)
        cell_scores.append(score)
        print(f'[+] Done with fold {idx}, final cell validation score: {score}')




    print(f'[+] CV complete:')
    print(f'\t avg gene score: {np.average(gene_scores)}')
    print(f'\t avg cell score: {np.average(cell_scores)}')
    print(f'\t diff: {np.average(gene_scores) - np.average(cell_scores)}')

    validation_by_moa[moa] = {
        'gene_score': np.average(gene_scores),
        'cell_score': np.average(cell_scores),
        'diff': np.average(gene_scores) - np.average(cell_scores)
    }


print(validation_by_moa)

with open('./validation_by_moa.json', 'w') as f:
    json.dump(validation_by_moa, f, indent=2)